PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
MODULE_MGM111A256V2
$EndINDEX
$MODULE MODULE_MGM111A256V2
Po 0 0 0 15 00000000 00000000 ~~
Li MODULE_MGM111A256V2
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -4.66424 -8.94311 1.00091 1.00091 0 0.05 N V 21 "MODULE_MGM111A256V2"
T1 -4.03189 8.78002 1.00172 1.00172 0 0.05 N V 21 "VAL**"
DS -6.45 7.5 -6.45 -7.5 0.127 27
DS -6.45 -7.5 6.45 -7.5 0.127 27
DS 6.45 -7.5 6.45 7.5 0.127 27
DS 6.45 7.5 -6.45 7.5 0.127 27
DP 0 0 0 0 4 0 24
Dl -3.80067 -7.5
Dl 3.8 -7.5
Dl 3.8 -4.0007
Dl -3.80067 -4.0007
DS -7.1 8 -7.1 -7.75 0.05 26
DS -7.1 -7.75 7.1 -7.75 0.05 26
DS 7.1 -7.75 7.1 8 0.05 26
DS 7.1 8 -7.1 8 0.05 26
DC -7.6 -6.9 -7.5 -6.9 0.2 21
DC -5.625 -6.773 -5.525 -6.773 0.2 27
DP 0 0 0 0 4 0 24
Dl -3.80327 -7.5
Dl 3.8 -7.5
Dl 3.8 -4.00344
Dl -3.80327 -4.00344
$PAD
Sh "1" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 -6.81
$EndPAD
$PAD
Sh "2" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 -5.61
$EndPAD
$PAD
Sh "3" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 -4.41
$EndPAD
$PAD
Sh "4" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 -3.21
$EndPAD
$PAD
Sh "5" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 -2.01
$EndPAD
$PAD
Sh "6" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 -0.81
$EndPAD
$PAD
Sh "7" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 0.39
$EndPAD
$PAD
Sh "8" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 1.59
$EndPAD
$PAD
Sh "9" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 2.79
$EndPAD
$PAD
Sh "10" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 3.99
$EndPAD
$PAD
Sh "11" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 5.19
$EndPAD
$PAD
Sh "12" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.65 6.39
$EndPAD
$PAD
Sh "13" R 2.4 0.8 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.58 6.53
$EndPAD
$PAD
Sh "14" R 2.4 0.8 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.38 6.53
$EndPAD
$PAD
Sh "15" R 2.4 0.8 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.18 6.53
$EndPAD
$PAD
Sh "16" R 2.4 0.8 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.02 6.53
$EndPAD
$PAD
Sh "17" R 2.4 0.8 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.22 6.53
$EndPAD
$PAD
Sh "18" R 2.4 0.8 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.42 6.53
$EndPAD
$PAD
Sh "19" R 2.4 0.8 0 0 2700
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.62 6.53
$EndPAD
$PAD
Sh "20" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 6.39
$EndPAD
$PAD
Sh "21" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 5.19
$EndPAD
$PAD
Sh "22" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 3.99
$EndPAD
$PAD
Sh "23" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 2.79
$EndPAD
$PAD
Sh "24" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 1.59
$EndPAD
$PAD
Sh "25" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 0.39
$EndPAD
$PAD
Sh "26" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 -0.81
$EndPAD
$PAD
Sh "27" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 -2.01
$EndPAD
$PAD
Sh "28" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 -3.21
$EndPAD
$PAD
Sh "29" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 -4.41
$EndPAD
$PAD
Sh "30" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 -5.61
$EndPAD
$PAD
Sh "31" R 2.4 0.8 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.65 -6.81
$EndPAD
$EndMODULE MODULE_MGM111A256V2
